(defrule augstow ""
	(population SMALL)
	(unemployment MEDIUM)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY LAKE)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY HEALTH_RESORT MALL FESTIVAL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY TOURISM INDUSTRY)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Augstow")
	))
)

(defrule biala_podlaska ""
	(population INTERMEDIATE)
	(unemployment MEDIUM)
	(university YES)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA FESTIVAL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Biala Podlaska")
	))
)

(defrule bialystok ""
	(population INTERMEDIATE)
	(unemployment MEDIUM)
	(university YES)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA OPERA SANCTUARY MALL STADIUM FESTIVAL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY SERVICES)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Bialystok")
	))
)

(defrule bielsko_biala ""
	(population INTERMEDIATE)
	(unemployment MEDIUM)
	(university YES)
	(terrain MOUNTAINS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA SANCTUARY MALL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY TOURISM SERVICES)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Bielsko-Biala")
	))
)

(defrule braniewo ""
	(population SMALL)
	(unemployment HIGH)
	(university NO)
	(terrain SEASIDE)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY DAM)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY ZOO)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY TOURISM)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Braniewo")
	))
)

(defrule bydgoszcz ""
	(population INTERMEDIATE)
	(unemployment MEDIUM)
	(university YES)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY RIVER)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA OPERA SANCTUARY STADIUM FESTIVAL ZOO)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY SERVICES)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN PLANE)))
	=>
	(assert (result
		(city "Bydgoszcz")
	))
)

(defrule bytom ""
	(population INTERMEDIATE)
	(unemployment HIGH)
	(university NO)
	(terrain MOUNTAINS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA OPERA SANCTUARY STADIUM)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN HIGHWAY)))
	=>
	(assert (result
		(city "Bytom")
	))
)

(defrule chelm ""
	(population INTERMEDIATE)
	(unemployment VHIGH)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA MONUMENT)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY SERVICES)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Chelm")
	))
)

(defrule chorzow ""
	(population INTERMEDIATE)
	(unemployment MEDIUM)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA ZOO MALL STADIUM FESTIVAL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY SERVICES)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN HIGHWAY)))
	=>
	(assert (result
		(city "Chorzow")
	))
)

(defrule ciechocinek ""
	(population SMALL)
	(unemployment HIGH)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY RIVER)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY HEALTH_RESORT FESTIVAL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY TOURISM)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Ciechocinek")
	))
)

(defrule czestochowa ""
	(population INTERMEDIATE)
	(unemployment HIGH)
	(university YES)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA MONUMENT SANCTUARY MALL STADIUM)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY SERVICES TOURISM)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Czestochowa")
	))
)

(defrule darlowo ""
	(population SMALL)
	(unemployment VHIGH)
	(university NO)
	(terrain SEASIDE)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY TOURISM)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Darlowo")
	))
)

(defrule elblag ""
	(population INTERMEDIATE)
	(unemployment VHIGH)
	(university NO)
	(terrain SEASIDE)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY DAM)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA SANCTUARY FESTIVAL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY SERVICES)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Elblag")
	))
)

(defrule elk ""
	(population INTERMEDIATE)
	(unemployment VHIGH)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY LAKE)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY SANCTUARY STADIUM CINEMA FESTIVAL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY SERVICES)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Elk")
	))
)

(defrule gdansk ""
	(population LARGE)
	(unemployment LOW)
	(university YES)
	(terrain SEASIDE)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY RIVER)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA OPERA ZOO MONUMENT SANCTUARY MALL STADIUM FESTIVAL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY TOURISM SERVICES)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN PLANE HIGHWAY)))
	=>
	(assert (result
		(city "Gdansk")
	))
)

(defrule gdynia ""
	(population INTERMEDIATE)
	(unemployment LOW)
	(university YES)
	(terrain SEASIDE)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA ZOO MALL STADIUM FESTIVAL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY TOURISM SERVICES)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN PLANE HIGHWAY)))
	=>
	(assert (result
		(city "Gdynia")
	))
)

(defrule gizycko ""
	(population SMALL)
	(unemployment VHIGH)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY LAKE)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY FESTIVAL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY TOURISM)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Gizycko")
	))
)

(defrule gliwice ""
	(population INTERMEDIATE)
	(unemployment LOW)
	(university YES)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY SANCTUARY CINEMA MALL STADIUM)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN HIGHWAY)))
	=>
	(assert (result
		(city "Gliwice")
	))
)

(defrule gniezno ""
	(population INTERMEDIATE)
	(unemployment HIGH)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY LAKE)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA MONUMENT SANCTUARY MALL STADIUM)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY SERVICES)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Gniezno")
	))
)

(defrule gorzow_wlkp ""
	(population INTERMEDIATE)
	(unemployment MEDIUM)
	(university YES)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY RIVER)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA ZOO FESTIVAL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY SERVICES)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Gorzow Wielkopolski")
	))
)

(defrule grudziadz ""
	(population INTERMEDIATE)
	(unemployment VHIGH)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY LAKE RIVER)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA MALL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY SERVICES)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN HIGHWAY)))
	=>
	(assert (result
		(city "Grudziadz")
	))
)

(defrule inowroclaw ""
	(population INTERMEDIATE)
	(unemployment MEDIUM)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY RIVER)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA HEALTH_RESORT)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY SERVICES)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Inowroclaw")
	))
)

(defrule jarocin ""
	(population SMALL)
	(unemployment MEDIUM)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA FESTIVAL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY SERVICES)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Jarocin")
	))
)

(defrule jelenia_gora ""
	(population INTERMEDIATE)
	(unemployment MEDIUM)
	(university NO)
	(terrain MOUNTAINS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY lAKE)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA MALL FESTIVAL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY SERVICES TOURISM)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Jelenia Gora")
	))
)

(defrule kalisz ""
	(population INTERMEDIATE)
	(unemployment LOW)
	(university YES)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA MALL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY SERVICES)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Kalisz")
	))
)

(defrule kamien_pomorski ""
	(population SMALL)
	(unemployment HIGH)
	(university NO)
	(terrain SEASIDE)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY DAM)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY HEALTH_RESORT)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY TOURISM SERVICES)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Kamien Pomorski")
	))
)

(defrule karpacz ""
	(population SMALL)
	(unemployment HIGH)
	(university NO)
	(terrain MOUNTAINS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY MALL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY TOURISM)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Karpacz")
	))
)

(defrule katowice ""
	(population INTERMEDIATE)
	(unemployment HIGH)
	(university YES)
	(terrain MOUNTAINS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA MALL STADIUM FESTIVAL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY SERVICES)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN PLANE HIGHWAY)))
	=>
	(assert (result
		(city "Katowice")
	))
)

(defrule kazimierz_dolny ""
	(population SMALL)
	(unemployment MEDIUM)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY RIVER)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY MONUMENT FESTIVAL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY TOURISM)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY)))
	=>
	(assert (result
		(city "Kazimierz Dolny")
	))
)

(defrule kedzierzyn_kozle ""
	(population INTERMEDIATE)
	(unemployment HIGH)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA MALL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Kedzierzyn Kozle")
	))
)

(defrule kielce ""
	(population INTERMEDIATE)
	(unemployment MEDIUM)
	(university YES)
	(terrain MOUNTAINS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA MALL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY SERVICES)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Kielce")
	))
)

(defrule kolobrzeg ""
	(population SMALL)
	(unemployment LOW)
	(university NO)
	(terrain SEASIDE)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY HEALTH_RESORT MALL FESTIVAL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY TOURISM)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Kolobrzeg")
	))
)

(defrule konin ""
	(population INTERMEDIATE)
	(unemployment MEDIUM)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA MALL STADIUM)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY SERVICES)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN HIGHWAY)))
	=>
	(assert (result
		(city "Konin")
	))
)

(defrule kostrzyn ""
	(population SMALL)
	(unemployment LOW)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA FESTIVAL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY SERVICES)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Kostrzyn")
	))
)

(defrule koszalin ""
	(population INTERMEDIATE)
	(unemployment LOW)
	(university YES)
	(terrain SEASIDE)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY LAKE)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA MALL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Koszalin")
	))
)

(defrule krakow ""
	(population LARGE)
	(unemployment LOW)
	(university YES)
	(terrain MOUNTAINS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY RIVER)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA OPERA ZOO MONUMENT SANCTUARY MALL STADIUM FESTIVAL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY SERVICES TOURISM)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN PLANE HIGHWAY)))
	=>
	(assert (result
		(city "Krakow")
	))
)

(defrule krapkowice ""
	(population SMALL)
	(unemployment HIGH)
	(university NO)
	(terrain MOUNTAINS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY RIVER)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY MONUMENT)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY TOURISM)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Krapkowice")
	))
)

(defrule kutno ""
	(population SMALL)
	(unemployment HIGH)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA MONUMENT)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN HIGHWAY)))
	=>
	(assert (result
		(city "Kutno")
	))
)

(defrule legnica ""
	(population INTERMEDIATE)
	(unemployment LOW)
	(university YES)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY LAKE)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA MALL FESTIVAL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY SERVICES)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN HIGHWAY)))
	=>
	(assert (result
		(city "Legnica")
	))
)

(defrule leszno ""
	(population INTERMEDIATE)
	(unemployment MEDIUM)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA ZOO MALL STADIUM FESTIVAL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY SERVICES)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Leszno")
	))
)

(defrule lublin ""
	(population INTERMEDIATE)
	(unemployment MEDIUM)
	(university YES)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY LAKE)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA OPERA MALL STADIUM)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY SERVICES)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN PLANE)))
	=>
	(assert (result
		(city "Lublin")
	))
)

(defrule leba ""
	(population SMALL)
	(unemployment HIGH)
	(university NO)
	(terrain SEASIDE)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY LAKE)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY MALL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY TOURISM)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Leba")
	))
)

(defrule lodz ""
	(population LARGE)
	(unemployment HIGH)
	(university YES)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA OPERA ZOO MALL STADIUM FESTIVAL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY SERVICES)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN PLANE HIGHWAY)))
	=>
	(assert (result
		(city "Lodz")
	))
)

(defrule malbork ""
	(population SMALL)
	(unemployment HIGH)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY RIVER)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA MONUMENT)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY TOURISM)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Malbork")
	))
)

(defrule mikolajki ""
	(population SMALL)
	(unemployment LOW)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY LAKE)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY TOURISM)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY)))
	=>
	(assert (result
		(city "Mikolajki")
	))
)

(defrule myslowice ""
	(population INTERMEDIATE)
	(unemployment LOW)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY DAM)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA MALL FESTIVAL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Myslowice")
	))
)

(defrule myslenice ""
	(population SMALL)
	(unemployment HIGH)
	(university NO)
	(terrain MOUNTAINS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY LAKE)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY MONUMENT)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY SERVIES)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Myslenice")
	))
)

(defrule naleczow ""
	(population SMALL)
	(unemployment MEDIUM)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY HEALTH_RESORT)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY TOURISM)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY)))
	=>
	(assert (result
		(city "Naleczow")
	))
)

(defrule nowa_sol ""
	(population SMALL)
	(unemployment HIGH)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Nowa sSol")
	))
)

(defrule nowy_sacz ""
	(population INTERMEDIATE)
	(unemployment MEDIUM)
	(university NO)
	(terrain MOUNTAINS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA MALL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY SERVICES)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Nowy Sacz")
	))
)

(defrule nowy_tomysl ""
	(population SMALL)
	(unemployment MEDIUM)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY ZOO)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY SERVICES)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN HIGHWAY)))
	=>
	(assert (result
		(city "Nowy Tomysl")
	))
)

(defrule nysa ""
	(population SMALL)
	(unemployment HIGH)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY LAKE)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA MONUMENT)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Nysa")
	))
)

(defrule olsztyn ""
	(population INTERMEDIATE)
	(unemployment LOW)
	(university YES)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY LAKE)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA FESTIVAL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY SERVICES)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Olsztyn")
	))
)

(defrule opole ""
	(population INTERMEDIATE)
	(unemployment MEDIUM)
	(university YES)
	(terrain MOUNTAINS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY RIVER)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY ZOO MALL CINEMA FESTIVAL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY SERVICES)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN HIGHWAY)))
	=>
	(assert (result
		(city "Opole")
	))
)

(defrule ostrowiec_swietokrzyski ""
	(population INTERMEDIATE)
	(unemployment HIGH)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA MALL STADIUM)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Ostrowiec Swietokrzyski")
	))
)

(defrule ostroda ""
	(population SMALL)
	(unemployment HIGH)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY LAKE)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY TOURISM)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Ostroda")
	))
)

(defrule ostrow_wlkp ""
	(population INTERMEDIATE)
	(unemployment MEDIUM)
	(university YES)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA MALL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY SERVICES)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Ostrow Wielkopolski")
	))
)

(defrule oswiecim ""
	(population SMALL)
	(unemployment LOW)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY MONUMENT SANCTUARY)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY TOURISM)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Oswiecim")
	))
)

(defrule pabianice ""
	(population INTERMEDIATE)
	(unemployment HIGH)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA MONUMENT MALL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Pabianice")
	))
)

(defrule pila ""
	(population INTERMEDIATE)
	(unemployment LOW)
	(university YES)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA MALL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY SERVICES)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Pila")
	))
)

(defrule piotrkow_tryb ""
	(population INTERMEDIATE)
	(unemployment MEDIUM)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA MALL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY SERVICES)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN HIGHWAY)))
	=>
	(assert (result
		(city "Piotrkow Trybunalski")
	))
)

(defrule plock ""
	(population INTERMEDIATE)
	(unemployment MEDIUM)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY RIVER)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA ZOO FESTIVAL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY SERVICES INDUSTRY)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Plock")
	))
)

(defrule polanica_zdroj ""
	(population SMALL)
	(unemployment MEDIUM)
	(university NO)
	(terrain MOUNTAINS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY RIVER)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY HEALTH_RESORT)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY TOURISM)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY)))
	=>
	(assert (result
		(city "Polanica Zdroj")
	))
)

(defrule poznan ""
	(population LARGE)
	(unemployment LOW)
	(university YES)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY RIVER LAKE)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA OPERA ZOO MONUMENT SANCTUARY FESTIVAL STADIUM MALL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY SERVICES)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN PLANE HIGHWAY)))
	=>
	(assert (result
		(city "Poznan")
	))
)

(defrule przemysl ""
	(population INTERMEDIATE)
	(unemployment VHIGH)
	(university NO)
	(terrain MOUNTAINS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA SANCTUARY)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY SERVICES)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN HIGHWAY)))
	=>
	(assert (result
		(city "Przemysl")
	))
)

(defrule rabka ""
	(population SMALL)
	(unemployment HIGH)
	(university NO)
	(terrain MOUNTAINS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY HEALTH_RESORT)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY TOURISM)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Rabka")
	))
)

(defrule radom ""
	(population INTERMEDIATE)
	(unemployment VHIGH)
	(university YES)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA MALL FESTIVAL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY SERVICES)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Radom")
	))
)

(defrule rybnik ""
	(population INTERMEDIATE)
	(unemployment LOW)
	(university YES)
	(terrain MOUNTAINS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA MALL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY SERVICES)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Rybnik")
	))
)

(defrule rzeszow ""
	(population INTERMEDIATE)
	(unemployment MEDIUM)
	(university YES)
	(terrain MOUNTAINS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA SANCTUARY MALL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY SERVICES)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN PLANE HIGHWAY)))
	=>
	(assert (result
		(city "Rzeszow")
	))
)

(defrule sandomierz ""
	(population SMALL)
	(unemployment MEDIUM)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY RIVER)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA MONUMENT)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY TOURISM)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Sandomierz")
	))
)

(defrule sanok ""
	(population SMALL)
	(unemployment MEDIUM)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY RIVER)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA MALL STADIUM)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Sanok")
	))
)

(defrule skarzysko_kamienna ""
	(population SMALL)
	(unemployment VHIGH)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY RIVER)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY SERVICES)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Skarzysko Kamienna")
	))
)

(defrule skoczow ""
	(population SMALL)
	(unemployment HIGH)
	(university NO)
	(terrain MOUNTAINS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY LAKE)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY FESTIVAL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY TOURISM)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Skoczow")
	))
)

(defrule sochaczew ""
	(population SMALL)
	(unemployment MEDIUM)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY MONUMENT)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY SERVICES)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Sochaczew")
	))
)

(defrule solina ""
	(population SMALL)
	(unemployment VHIGH)
	(university NO)
	(terrain MOUNTAINS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY DAM)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY)))
	=>
	(assert (result
		(city "Solina")
	))
)

(defrule sopot ""
	(population SMALL)
	(unemployment LOW)
	(university YES)
	(terrain SEASIDE)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY HEALTH_RESORT FESTIVAL MALL CINEMA OPERA)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY TOURISM SERVICES)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN HIGHWAY)))
	=>
	(assert (result
		(city "Sopot")
	))
)

(defrule stargard_szczecinski ""
	(population INTERMEDIATE)
	(unemployment VHIGH)
	(university YES)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY RIVER)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA MALL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Stargard Szczecinski")
	))
)

(defrule swinoujscie ""
	(population SMALL)
	(unemployment HIGH)
	(university NO)
	(terrain SEASIDE)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY DAM)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA HEALTH_RESORT)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY TOURISM SERVICES)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Swinoujscie")
	))
)

(defrule szczecin ""
	(population INTERMEDIATE)
	(unemployment MEDIUM)
	(university YES)
	(terrain SEASIDE)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY RIVER DAM)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA OPERA SANCTUARY MALL STADIUM)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY SERVICES)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN PLANE)))
	=>
	(assert (result
		(city "Szczecin")
	))
)

(defrule szczecinek ""
	(population SMALL)
	(unemployment VHIGH)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY LAKE)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY SERVICES)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Szczecinek")
	))
)

(defrule szklarska_poreba ""
	(population SMALL)
	(unemployment HIGH)
	(university NO)
	(terrain MOUNTAINS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY TOURISM)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Szklarska Poreba")
	))
)

(defrule tarnobrzeg ""
	(population SMALL)
	(unemployment MEDIUM)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY DAM)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA MALL STADIUM)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY SERVICES)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Tarnobrzeg")
	))
)

(defrule tarnow ""
	(population INTERMEDIATE)
	(unemployment MEDIUM)
	(university YES)
	(terrain MOUNTAINS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY MONUMENT CINEMA STADIUM MALL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY SERVICES)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN HIGHWAY)))
	=>
	(assert (result
		(city "Tarnow")
	))
)

(defrule tczew ""
	(population INTERMEDIATE)
	(unemployment HIGH)
	(university NO)
	(terrain SEASIDE)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY RIVER)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY MONUMENT)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY TOURISM)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Tczew")
	))
)

(defrule torun ""
	(population INTERMEDIATE)
	(unemployment MEDIUM)
	(university YES)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY RIVER)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA ZOO MONUMENT MALL STADIUM)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY SERVICES)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN HIGHWAY)))
	=>
	(assert (result
		(city "Torun")
	))
)

(defrule ustka ""
	(population SMALL)
	(unemployment VHIGH)
	(university NO)
	(terrain SEASIDE)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA HEALTH_RESORT)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY TOURISM)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Ustka")
	))
)

(defrule ustrzyki_dolne ""
	(population SMALL)
	(unemployment HIGH)
	(university NO)
	(terrain MOUNTAINS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY TOURISM)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Ustrzyki Dolne")
	))
)

(defrule walbrzych ""
	(population INTERMEDIATE)
	(unemployment VHIGH)
	(university YES)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA MALL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Walbrzych")
	))
)

(defrule warszawa ""
	(population LARGE)
	(unemployment LOW)
	(university YES)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY RIVER)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA OPERA ZOO MONUMENT SANCTUARY MALL STADIUM FESTIVAL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY TOURISM SERVICES INDUSTRY)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN PLANE HIGHWAY)))
	=>
	(assert (result
		(city "Warszawa")
	))
)

(defrule wieliczka ""
	(population SMALL)
	(unemployment LOW)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY MONUMENT)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY TOURISM)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Wieliczka")
	))
)

(defrule wielun ""
	(population SMALL)
	(unemployment MEDIUM)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY MONUMENT SANCTUARY)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY SERVICES)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY)))
	=>
	(assert (result
		(city "Wielun")
	))
)

(defrule wisla ""
	(population SMALL)
	(unemployment HIGH)
	(university NO)
	(terrain MOUNTAINS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY TOURISM)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Wisla")
	))
)

(defrule wloclawek ""
	(population INTERMEDIATE)
	(unemployment MEDIUM)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY RIVER)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA MALL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Wloclawek")
	))
)

(defrule wolsztyn ""
	(population SMALL)
	(unemployment MEDIUM)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY LAKE)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY HEALTH_RESORT)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY SERVICES)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Wolsztyn")
	))
)

(defrule wroclaw ""
	(population LARGE)
	(unemployment LOW)
	(university YES)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY RIVER)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY STADIUM CINEMA OPERA ZOO MONUMENT SANCTUARY MALL FESTIVAL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY TOURISM SERVICES)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN PLANE HIGHWAY)))
	=>
	(assert (result
		(city "Wroclaw")
	))
)

(defrule zakopane ""
	(population SMALL)
	(unemployment MEDIUM)
	(university NO)
	(terrain MOUNTAINS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA SANCTUARY FESTIVAL MALL)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY TOURISM SERVICES)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Zakopane")
	))
)

(defrule zamosc ""
	(population INTERMEDIATE)
	(unemployment HIGH)
	(university NO)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY LAKE)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA ZOO)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY SERVICES)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Zamosc")
	))
)

(defrule zielona_gora ""
	(population SMALL)
	(unemployment MEDIUM)
	(university YES)
	(terrain MIDLANDS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA MALL STADIUM)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY INDUSTRY SERVICES)))
	(hospital YES)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN PLANE)))
	=>
	(assert (result
		(city "Zielona Gora")
	))
)

(defrule zywiec ""
	(population SMALL)
	(unemployment MEDIUM)
	(university NO)
	(terrain MOUNTAINS)
	(landmarks $?l)
	(test (subsetp $?l (create$ ANY LAKE)))
	(activities $?a)
	(test (subsetp $?a (create$ ANY CINEMA)))
	(employment $?e)
	(test (subsetp $?e (create$ ANY TOURISM INDUSTRY)))
	(hospital NO)
	(transportation $?t)
	(test (subsetp $?t (create$ ANY TRAIN)))
	=>
	(assert (result
		(city "Zywiec")
	))
)