(deftemplate currentState
	(slot name (default none))
	(slot state (default middle))
	(slot text)		
	(slot answerType (default radioButton))
	(multislot selected)
	(multislot answers)
)


(deftemplate result
	(slot city (type STRING))
)

; Startup window
(defrule system-banner ""
	=>
	(assert	(currentState 
		(text START_TEXT)
		(name start)
		(state initial)
		(answers))
	)
)

; Results window
(defrule EndOfQuestions ""
	?state<-(currentState)
	(end)
	=>
	(retract ?state) 
	(assert (currentState 
		(text NO_RESULTS_END_TEXT)
		(state final)
	))
)

(defrule selectOccupation ""
	?state<-(currentState)
	(not(occupationQuestion))
	=>
	(retract ?state) 
	(assert (occupationQuestion))	
	(assert (currentState
		(text QUESTION_JOB)
		(name job)
		(answers STUDENT WORKER UNEMPLOYED RETIRED)
		(selected STUDENT)	
	))
)

(defrule wannaStudy ""
	?state<-(currentState)
	?i<-(job STUDENT|UNEMPLOYED)
	(not(studiesQuestion))
	=>
	(retract ?state) 
	(assert (studiesQuestion))	
	(assert (hospital YES))
	(assert (hospital NO))
	(assert (currentState
		(name study)	
		(text QUESTION_UNIVERSITY)
		(answers YES NO)
		(selected YES)	
	))
)

(defrule wantToStudy ""
	?i<-(job STUDENT|UNEMPLOYED)
	(study YES)
	=>
	(retract ?i)
	(assert (unemployment LOW))
	(assert (unemployment MEDIUM))
	(assert (unemployment HIGH))
	(assert (unemployment VHIGH))
	(assert (university YES))
	(assert (employment ANY))
)

(defrule dontWantToStudyStudent ""
	?i<-(job STUDENT)
	(study NO)
	=>
	(retract ?i)
	(assert (unemployment LOW))
	(assert (unemployment MEDIUM))
	(assert (unemployment HIGH))
	(assert (unemployment VHIGH))
	(assert (university YES))
	(assert (university NO))
	(assert (employment ANY))
)

(defrule dontWantToStudyUnemployed ""
	?i<-(job UNEMPLOYED)
	(study NO)
	=>
	(assert (unemployment LOW))
	(assert (unemployment MEDIUM))
	(assert (university YES))
	(assert (university NO))
	(assert (askWannaWork))
)

(defrule wannaWork ""
	?state<-(currentState)
	?i<-(askWannaWork)
	=>
	(retract ?state) 
	(retract ?i)
	(assert (currentState
		(name work)	
		(text QUESTION_START_JOB)
		(answers YES NO)
		(selected YES)	
	))
)	

(defrule isWorker ""
	?i<-(job WORKER)
	=>
	(assert (unemployment LOW))
	(assert (unemployment MEDIUM))
	(assert (unemployment HIGH))
	(assert (university YES))
	(assert (university NO))
	(assert (hospital YES))
	(assert (hospital NO))
)

(defrule isRetired ""
	?i<-(job RETIRED)
	=>
	(assert (unemployment LOW))
	(assert (unemployment MEDIUM))
	(assert (unemployment HIGH)) 
	(assert (unemployment VHIGH)) 
	(assert (university YES))
	(assert (university NO))
	(assert (employment ANY))
)


(defrule workBrandsWorker ""
	?state<-(currentState)
	?i<-(job WORKER)
	=>
	(retract ?state) 
	(retract ?i)
	(assert (currentState
		(name employment)	
		(text QUESTION_EMPLOYMENT)
		(answers ANY INDUSTRY SERVICES TOURISM)
		(selected ANY)	
	))
)

(defrule workBrandsUnemployed ""
	?state<-(currentState)
	?i<-(job UNEMPLOYED)
	(work YES)
	=>
	(retract ?state) 
	(retract ?i)
	(assert (currentState
		(name employment)	
		(text QUESTION_EMPLOYMENT)
		(answers ANY INDUSTRY SERVICES TOURISM)
		(selected ANY)	
	))
)

(defrule healthCare ""
	?state<-(currentState)
	?i<-(job RETIRED)
	=>
	(retract ?state)
	(retract ?i)
	(assert (currentState
		(name hospital)
		(text QUESTION_HEALTH_SUPPORT)
		(answers YES NO)
		(selected YES)
	))
)

(defrule citySize ""
 	?state<-(currentState)
	(not(citySize))
=>
	(retract ?state)
	(assert (citySize))
	(assert (currentState
		(name size)
		(text QUESTION_POPULATION)
		(answers ANY LARGE INTERMEDIATE SMALL)
		(selected ANY)
	))
)

(defrule citySize_any ""
	?i<-(size ANY)
	=>
	(retract ?i)
	(assert (population SMALL))
	(assert (population INTERMEDIATE))
	(assert (population LARGE))
)

(defrule citySize_small ""
	?i<-(size SMALL)
	=>
	(retract ?i)
	(assert (population SMALL))
)

(defrule citySize_intermediate ""
	?i<-(size INTERMEDIATE)
=>
	(retract ?i)
	(assert (population INTERMEDIATE))
)

(defrule citySize_large ""
	?i<-(size LARGE)
=>
	(retract ?i)
	(assert (population LARGE))
)

(defrule landscapeValors ""
 	?state<-(currentState)
	(not(landValors))
=>
	(retract ?state)
	(assert (landValors))
	(assert (currentState
		(name landmarks)
		(text QUESTION_LANDSCAPES)
		(answerType checkBox)
		(answers LAKE RIVER DAM)
		(selected)
	))
)

(defrule terrainNearby ""
 	?state<-(currentState)
	(not(terrainNearby))
=>
	(retract ?state)
	(assert (terrainNearby))
	(assert (currentState
		(name partOfPoland)
		(text QUESTION_TERRAIN)
		(answers ANY SEASIDE MIDLANDS MOUNTAINS)
		(selected ANY)
	))
)

(defrule partOfPoland_any ""
	?i<-(partOfPoland ANY)
	=>
	(retract ?i)
	(assert (terrain SEASIDE))
	(assert (terrain MIDLANDS))
	(assert (terrain MOUNTAINS))
)

(defrule partOfPoland_seaside ""
	?i<-(partOfPoland SEASIDE)
	=>
	(retract ?i)
	(assert (terrain SEASIDE))
)

(defrule partOfPoland_midlands ""
	?i<-(partOfPoland MIDLANDS)
=>
	(retract ?i)
	(assert (terrain MIDLANDS))
)

(defrule partOfPoland_mountains ""
	?i<-(partOfPoland MOUNTAINS)
=>
	(retract ?i)
	(assert (terrain MOUNTAINS))
)

(defrule chooseTransports ""
	?state<-(currentState)
	(not(transportQuestion))
	=>
	(retract ?state)
	(assert (transportQuestion))
	(assert (currentState
		(name transportation)
		(text QUESTION_TRANSPORT_TYPES)
		(answerType checkBox)
		(answers TRAIN HIGHWAY PLANE)
		(selected)
	))
)

(defrule favouriteActivities ""
	?state<-(currentState)
	(not(favActivities))
	=>
	(retract ?state)
	(assert (favActivities))
	(assert (currentState
		(name activities)
		(text QUESTION_ACTIVITIES)
		(answerType checkBox)
		(answers CINEMA MALL FESTIVAL SANCTUARY OPERA ZOO STADIUM HEALTH_RESORT MONUMENT)
		(selected)
	))
	(assert (end))
)